package pl.eatsApi.eats.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductsController {

    @RequestMapping("/")
    public String index() {
        return "Proudly handcrafted by " +
                "Dzwonk :)";
    }

    @RequestMapping("/products")
    public List<String> getProducts(){
        ArrayList<String> products=new ArrayList<>();
        products.add("product1");
        products.add("product2");

        return products;
    }

    @RequestMapping("/favorite_products")
    public List<String> getFavoriteProducts(){
        ArrayList<String> products=new ArrayList<>();
        products.add("FavoriteProduct1");
        products.add("FavoriteProduct2");

        return products;
    }
}
