package com.dzwonk.eats.repositories

import com.dzwonk.eats.api.FavoriteOffersApi
import io.reactivex.Single

class FavoriteOffersRepository constructor(private val favoriteOffersApi: FavoriteOffersApi) {
    fun getFavoriteOffers(): Single<List<String>> = favoriteOffersApi.getFavoriteProducts()
}