package com.dzwonk.eats.repositories

import com.dzwonk.eats.api.OffersApi
import io.reactivex.Single

class OffersRepository constructor(private val offersApi: OffersApi) {
    fun getOffers(): Single<List<String>> = offersApi.getEatsProducts()
}