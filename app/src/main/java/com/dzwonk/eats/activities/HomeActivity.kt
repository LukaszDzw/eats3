package com.dzwonk.eats.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.dzwonk.eats.R
import com.dzwonk.eats.enums.FragmentsEnum
import com.dzwonk.eats.fragments.FavoritesOffersFragment
import com.dzwonk.eats.fragments.OffersFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class HomeActivity : AppCompatActivity() {
    private var fragmentsEnumHashMap: HashMap<Int, FragmentsEnum> = HashMap()

    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            item.isChecked = true

            if (fragmentsEnumHashMap.containsKey(item.itemId)) {
                val fragmentsEnum = fragmentsEnumHashMap[item.itemId]
                    ?: return@OnNavigationItemSelectedListener false

                replaceFragment(fragmentsEnum)
            }

            true
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentsEnumHashMap[R.id.navigation_offers] = FragmentsEnum.OffersTag
        fragmentsEnumHashMap[R.id.navigation_favorite_offers] = FragmentsEnum.FavoritesOffersTag

        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        replaceFragment(FragmentsEnum.OffersTag)
    }

    private fun getFragmentForTag(fragmentsEnum: FragmentsEnum): Fragment? {
        val fragment = supportFragmentManager.findFragmentByTag(fragmentsEnum.name)

        if (fragment != null) {
            return supportFragmentManager.findFragmentByTag(fragmentsEnum.name)
        }

        return when (fragmentsEnum) {
            FragmentsEnum.OffersTag -> OffersFragment.newInstance()
            FragmentsEnum.FavoritesOffersTag -> FavoritesOffersFragment.newInstance()
        }
    }

    private fun replaceFragment(fragmentsEnum: FragmentsEnum) {
        val fragment = getFragmentForTag(fragmentsEnum) ?: return;

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.contentRoot, fragment, fragmentsEnum.name)
            .addToBackStack(fragmentsEnum.name)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .commit()
    }
}
