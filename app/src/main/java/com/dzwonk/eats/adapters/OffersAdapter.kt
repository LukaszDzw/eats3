package com.dzwonk.eats.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dzwonk.eats.R
import kotlinx.android.synthetic.main.my_text_view.view.*

class OffersAdapter(
    val items: ArrayList<String>,
    private val context: Context,
    private val clickListener: ItemClickListener
) :
    RecyclerView.Adapter<ViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.my_text_view, parent, false))
    }

    override fun onBindViewHolder(parent: ViewHolder, position: Int) {
        parent.myTextView.text = items[position]
        parent.myTextView.setOnClickListener { l -> clickListener.onOffersItemClick(l, position) }
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val myTextView = view.my_text
}

interface ItemClickListener {
    fun onOffersItemClick(view: View, position: Int)
}