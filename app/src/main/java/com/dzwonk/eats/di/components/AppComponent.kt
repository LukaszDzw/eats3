package com.dzwonk.eats.components

import android.app.Application
import android.content.Context
import com.dzwonk.eats.EatsApplication
import com.dzwonk.eats.modules.ActivityModule
import com.dzwonk.eats.modules.EatsApiRetrofitModule
import com.dzwonk.eats.modules.FragmentModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ActivityModule::class, FragmentModule::class, EatsApiRetrofitModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(eatsApplication: EatsApplication);
}
