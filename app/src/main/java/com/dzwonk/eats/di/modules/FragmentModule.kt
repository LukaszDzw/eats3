package com.dzwonk.eats.modules

import com.dzwonk.eats.fragments.FavoritesOffersFragment
import com.dzwonk.eats.fragments.OffersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector(modules = [OffersRepositoryModule::class])
    internal abstract fun contributeOffersFragment(): OffersFragment

    @ContributesAndroidInjector(modules = [OffersRepositoryModule::class])
    internal abstract fun contributeFavoritesOffersFragment(): FavoritesOffersFragment
}