package com.dzwonk.eats.modules

import com.dzwonk.eats.api.FavoriteOffersApi
import com.dzwonk.eats.api.OffersApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module(includes = [EatsApiRetrofitModule::class])
class EatsApiModule {
    @Provides
    fun offersApi(retrofit: Retrofit) : OffersApi = retrofit.create(OffersApi::class.java);

    @Provides
    fun favoriteOffersApi(retrofit: Retrofit) : FavoriteOffersApi = retrofit.create(FavoriteOffersApi::class.java);
}