package com.dzwonk.eats.modules

import com.dzwonk.eats.api.FavoriteOffersApi
import com.dzwonk.eats.api.OffersApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module(includes = [OkHttpClientModule::class])
class EatsApiRetrofitModule {

    @Provides
    fun retrofit(okHttpClient: OkHttpClient,
                 gsonConverterFactory: GsonConverterFactory) : Retrofit = Retrofit.Builder()
                     .client(okHttpClient)
                     .baseUrl("http://192.168.2.171:8080")
                     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                     .addConverterFactory(gsonConverterFactory)
                     .build()

    @Provides
    fun gson() : Gson = GsonBuilder().create()

    @Provides
    fun gsonConverterFactory(gson:Gson):GsonConverterFactory = GsonConverterFactory.create(gson)
}