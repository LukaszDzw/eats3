package com.dzwonk.eats.modules

import com.dzwonk.eats.api.FavoriteOffersApi
import com.dzwonk.eats.api.OffersApi
import com.dzwonk.eats.repositories.FavoriteOffersRepository
import com.dzwonk.eats.repositories.OffersRepository
import dagger.Module
import dagger.Provides

@Module(includes = [EatsApiModule::class])
class OffersRepositoryModule {
    @Provides
    fun getOffersRepository(offersApi: OffersApi): OffersRepository = OffersRepository(offersApi)

    @Provides
    fun getFavoriteOffersRepository(favoriteOffersApi: FavoriteOffersApi): FavoriteOffersRepository =
        FavoriteOffersRepository(favoriteOffersApi)
}