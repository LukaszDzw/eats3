package com.dzwonk.eats.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.dzwonk.eats.R
import com.dzwonk.eats.adapters.ItemClickListener
import com.dzwonk.eats.adapters.OffersAdapter
import com.dzwonk.eats.repositories.FavoriteOffersRepository
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_favorites_offers.*
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FavoritesOffersFragment : Fragment(), ItemClickListener {

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    @Inject
    lateinit var favoriteOffersRepository: FavoriteOffersRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorites_offers, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onActivityCreated(savedInstanceState)

        val mLayoutManager = LinearLayoutManager(context)

        favorite_offers_recycler_view.layoutManager = mLayoutManager;

        val context: Context = this.context ?: return
        val adapter = OffersAdapter(arrayListOf(), context, this)
        favorite_offers_recycler_view.adapter = adapter;

        var result = favoriteOffersRepository
            .getFavoriteOffers()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ list ->
                run {
                    adapter.items.clear()
                    adapter.items.addAll(list)
                    adapter.notifyDataSetChanged()
                }
            },
                { error -> Toast.makeText(context, error.message, Toast.LENGTH_LONG).show() }
            )
    }

    override fun onOffersItemClick(view: View, position: Int) {
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            FavoritesOffersFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
