package com.dzwonk.eats.fragments


import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.mvrx.fragmentViewModel
import com.dzwonk.eats.R
import com.dzwonk.eats.adapters.ItemClickListener
import com.dzwonk.eats.adapters.OffersAdapter
import com.dzwonk.eats.repositories.OffersRepository
import com.dzwonk.eats.viewmodels.MvRxViewModel
import com.dzwonk.eats.viewmodels.OffersState
import com.dzwonk.eats.viewmodels.OffersViewModel
import com.dzwonk.eats.views.textRow
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_offers.*
import javax.inject.Inject


class OffersFragment : BaseFragment(), ItemClickListener {

    @Inject
    lateinit var offersRepository: OffersRepository

    override val epoxyRecyclerView: Int
        get() = R.id.offersRecyclerView

    private val testSubject = PublishSubject.create<Int>()

    override val layoutId: Int
        get() = R.layout.fragment_offers

    private val viewModell : OffersViewModel by fragmentViewModel()

    override fun epoxyController() = simpleController(viewModell) { state->
        textRow {
            id("texcik xd")
            text(state.offers.toString())
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onActivityCreated(savedInstanceState)

        val mLayoutManager = LinearLayoutManager(context)
        offers_recycler_view.layoutManager = mLayoutManager;

        val context: Context = this.context ?: return

        val adapter = OffersAdapter(arrayListOf(), context, this);
        offers_recycler_view.adapter = adapter;

        var result = offersRepository
            .getOffers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { t ->
                val xdd = t.toMutableList()
                xdd.add("xdd")
                xdd
            }
            .subscribe(
                { list ->
                    run {
                        adapter.items.clear()
                        adapter.items.addAll(list)
                        adapter.notifyDataSetChanged()
                    }
                },
                { error -> Toast.makeText(context, error.message, Toast.LENGTH_LONG).show() }
            )
    }

    override fun onOffersItemClick(view: View, position: Int) {
        Toast.makeText(context, "test", Toast.LENGTH_SHORT).show()

        testSubject.onNext(position)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            OffersFragment().apply {
            }
    }
}
