package com.dzwonk.eats

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import com.dzwonk.eats.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class EatsApplication : Application(), HasActivityInjector, HasSupportFragmentInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().application(this).context(applicationContext).build().inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector;

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector
}