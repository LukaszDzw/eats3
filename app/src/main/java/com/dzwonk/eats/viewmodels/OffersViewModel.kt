package com.dzwonk.eats.viewmodels

import androidx.lifecycle.ViewModel
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import com.dzwonk.eats.repositories.OffersRepository

data class OffersState(
    val offers : List<String> = listOf()
) : MvRxState

class OffersViewModel(initialState: OffersState, val offersRepository: OffersRepository) : MvRxViewModel<OffersState>(initialState) {

    init{

    }

    val text = "DATA Binding offers text"

    fun getOffers(){

    }

    companion object : MvRxViewModelFactory<OffersViewModel, OffersState> {

        override fun create(viewModelContext: ViewModelContext, state: OffersState): OffersViewModel {
            return OffersViewModel(state)
        }
    }
}