package com.dzwonk.eats.api

import io.reactivex.Single
import retrofit2.http.GET

interface OffersApi {
    @GET("/products")
    fun getEatsProducts():Single<List<String>>
}
