package com.dzwonk.eats.api

import io.reactivex.Single
import retrofit2.http.GET

interface FavoriteOffersApi {
    @GET("/favorite_products")
    fun getFavoriteProducts(): Single<List<String>>
}